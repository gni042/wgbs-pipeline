#!/bin/bash


#### FUNCTIONS
usage()
{
    echo ""
    echo "USAGE:"
    echo "$0 [OPTIONS] -o OUTPUT_FOLDER FASTQ_FILES [...]"
    echo ""
    echo "OPTIONS"
    echo "   -o DIR"
    echo "          Output directory [REQUIRED]"
    echo "   -f"
    echo "          Overwrite previous results, if any."
    echo "   -m"
    echo "          Monitor RAM and CPU usage."
    echo "   -u INTEGER"
    echo "          Subset reads to test pipeline (to INTEGER)."
    echo "   -s STRATEGIES"
    echo "          Only run selected strategies. Possible choices of STRATEGIES: "1", "2", "3""
    echo "          or any combination of them, comma-separated (e.g. "1,2,3", "2,3")."
    echo "   -h"
    echo "          Print (this) help message."
    echo ""
}

###### PARSE CMDLINE

output_dir=
FORCE=0
MONITOR=0
SUBSET=0
FILES=
# Strategies to perform
S1=true
S2=true
S3=true

while getopts ":o:fmu:s:h" opt; do
    case ${opt} in 
        o)
            output_dir=$OPTARG
            ;;
        f)
            FORCE=1
            ;;
        m)
            MONITOR=1
            ;;
        u)
            SUBSET=$OPTARG
            ;;
        s)
            S1=false
            S2=false
            S3=false
            for s in $( echo $OPTARG | tr "," " " ); do
                case $s in 
                    1)
                        S1=true
                        ;;
                    2)
                        S2=true
                        ;;
                    3)
                        S3=true
                        ;;
                    *)
                        echo "[ERROR] Strategies can only be 1, 2, 3"
                        exit 1
                        ;;
                esac
            done
            ;;
        h)
            usage
            exit 0
            ;;
        \?)
            echo "[ERROR] Invalid option: -$OPTARG" >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
        :)
            echo "[ERROR] Option -$OPTARG requires an argument." >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
    esac
done
shift $((OPTIND -1))

FILES=($@)


if [[ "$output_dir" == "" ]]; then
    echo "[ERROR] Output folder (-o option) is required"
    exit 1
fi
if [[ -f ${output_dir} ]]; then
    echo "[ERROR] The output folder provided (\"${output_dir}\") is actually a file. Aborting."
    exit 1
fi

if [[ ${#FILES[@]} < 2 ]]; then
    echo "[ERROR] At least 2 fastq files need to be provided"
    echo "        (Try $0 -h for help)"
    exit 1
fi



# Some variables
MT_chr_name="chrM"
#LAMBDA_chr_name="NC_001416.1"
LAMBDA_chr_name="chrL"

# Get pipeline folder
PIPELINEPATH="$( cd "$(dirname "$0")" > /dev/null 2>&1 ; pwd -P )"

# Reference genome (with Bismark indices and stuff
ref_dir="${PIPELINEPATH}/ref"
b38_ref="${ref_dir}/GRCh38"
nuc_ref="${ref_dir}/GRCh38_no_mt"
mt_ref="${ref_dir}/chrM"



# Programs
bismark="/usr/local/bin/bismark-0.22.3"
deduplicate_bismark="/usr/local/bin/deduplicate_bismark-0.22.3"
bismark_meth_extract="/usr/local/bin/bismark_methylation_extractor-0.22.3"
bowtie2="/usr/local/bin/bowtie2-2.4.1"
bowtie_path="/opt/bowtie2-2.4.1/"
samtools="/usr/local/bin/samtools-1.10"
samtools_path="/opt/samtools-1.10"
ext_chr_srt="${PIPELINEPATH}/apps/ext_chr_srt.py"
meth_prop="${PIPELINEPATH}/apps/methylation_proportion.py"
generate_bs_indices="${PIPELINEPATH}/apps/generate_bs_indices.sh"

psrecord="psrecord"

########################################
#>>>>           OPTIONS           <<<<<#
########################################

# To activate CPU and RAM monitoring
if [[ $MONITOR > 0 ]]; then
    MONITOR=true
else
    MONITOR=false
fi

# To subset reads
if [[ $SUBSET > 0 ]]; then
    SUBSET_READS=true
else
    SUBSET_READS=false
fi

########################################

# Check that both MT chromosome and lambda are present in the reference
if ! grep -m1 -q "^>${MT_chr_name}" ${b38_ref}/Bisulfite_Genome/CT_conversion/genome_mfa.CT_conversion.fa; then
    echo "[ERROR] \"${MT_chr_name}\" does not exist in ${b38_ref}"
    exit 1
fi
if grep -m1 -q "^>${MT_chr_name}" ${nuc_ref}/Bisulfite_Genome/CT_conversion/genome_mfa.CT_conversion.fa; then
    echo "[ERROR] \"${MT_chr_name}\" is present in the nuclear-only reference: ${nuc_ref}"
    exit 1
fi
if ! grep -m1 -q "^>${MT_chr_name}" ${mt_ref}/Bisulfite_Genome/CT_conversion/genome_mfa.CT_conversion.fa; then
    echo "[ERROR] \"${MT_chr_name}\" does not exist in ${mt_ref}"
    exit 1
fi
if ! grep -m1 -q "^>${LAMBDA_chr_name}" ${b38_ref}/Bisulfite_Genome/CT_conversion/genome_mfa.CT_conversion.fa; then
    echo "[ERROR] \"${LAMBDA_chr_name}\" does not exist in ${b38_ref}"
    exit 1
fi


# Exit when any command fails
set -e
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
trap 'if [ "$?" -ne "0" ]; then echo "\"${last_command}\" command filed with exit code different from 0"; fi' EXIT

# Get number of processors in the machine
N_proc=$( grep -c processor /proc/cpuinfo )
# Get RAM in the machine
RAM=$( free -g | grep '^Mem' | awk '{print $2}' )

# Ensure reference files/indices are there, download and index otherwise
if [[ ! -s ${b38_ref}/Bisulfite_Genome/GA_conversion/genome_mfa.GA_conversion.fa ]]; then
    echo "[WARNING] Reference files not found. Downloading and indexing... (THIS SHOULD TAKE A WHILE)"
    bash ${generate_bs_indices} ${ref_dir} > ${ref_dir}.stdout.log 2> ${ref_dir}.stderr.log
fi


declare -a R1
declare -a R2

# Get fastq file inputs
for f in "${FILES[@]}"; do
    if [[ ! -f ${f} ]]; then
        echo "[ERROR] Input file \"${f}\" does not exist"
        exit 1
    fi
    if [[ ${f} == *.R1.fastq.gz ]]; then
        R1+=(${f})
    elif [[ ${f} == *.R2.fastq.gz ]]; then
        R2+=(${f})
    else
        echo "[ERROR] Input file \"${f}\" needs to have a \".R1.fastq.gz\" or \".R2.fastq.gz\" extension"
        exit 1
    fi
done

if [[ ${#R1[@]} -lt 1 || ${#R2[@]} -lt 1 ]]; then
    echo "[ERROR] Forward (R1) and reverse (R2) files are required (at least one of each)"
    exit 1
fi

# Unique the files, just in case
R1=($(echo "${R1[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
R2=($(echo "${R2[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))

# Get sample id
sample_id=$( basename ${R1[0]%.R1.fastq.gz} )

# Create output folder if it does not exist
if [[ ! -d ${output_dir} ]]; then
    echo "[WARNING] creating output directory \"${output_dir}\" (did not exist)"
    mkdir -p ${output_dir}
fi
# Create directory for sample
output_dir=${output_dir}/${sample_id}
if [[ -d ${output_dir} && $(ls -A ${output_dir}) ]] ; then
    if [[ $FORCE > 0 ]]; then
        echo "[WARNING] sample directory (\"${output_dir}\") already exists and it's not empty. Files may be overwritten!"
    else
        echo "[ERROR] sample directory (\"${output_dir}\") already exists and it's not empty. Remove dir or use -f option"
    fi
fi

mkdir -p ${output_dir}
log_dir=${output_dir}/logs
mkdir -p ${log_dir}


# Print input data :)
start_date=$( date )
echo
echo "Analysis started:"
echo ${start_date}
echo
echo "=================================================="
echo "SAMPLE ID: \"${sample_id}\""
echo "=================================================="
echo "R1 FILES:"
for f in ${R1[@]}; do
    echo "  $f"
done
echo "R2 FILES:"
for f in ${R2[@]}; do
    echo "  $f"
done
echo "OUTPUT DIR: \"${output_dir}\""
echo "=================================================="
echo

# Comma-separate input files for Bismark command
printf -v R1 '%s,' "${R1[@]}"
R1=$( echo "${R1%,}" )
printf -v R2 '%s,' "${R2[@]}"
R2=$( echo "${R2%,}" )



if $MONITOR; then
    echo "MONITORING CPU AND RAM..."
    echo
fi

if $SUBSET_READS; then
    subset_opt="-u ${SUBSET}"
    echo "SUBSETTING TO ${SUBSET} READS ONLY!!!!"
    echo
fi

if [[ $S1 == "true" ]]; then
    echo "**** WILL PERFORM STRATEGY 1 ****"
else
    echo "**** WILL NOT PERFORM STRATEGY 1 ****"
fi
if [[ $S2 == "true" ]]; then
    echo "**** WILL PERFORM STRATEGY 2 ****"
else
    echo "**** WILL NOT PERFORM STRATEGY 2 ****"
fi
if [[ $S3 == "true" ]]; then
    echo "**** WILL PERFORM STRATEGY 3 ****"
else
    echo "**** WILL NOT PERFORM STRATEGY 3 ****"
fi

if [[ $S1 == "true" ]]; then
    # Create TMP folder
    tmp_dir=${output_dir}/tmp_1
    mkdir -p ${tmp_dir}
    echo "##################################################"
    echo "#                   STRATEGY 1                   #"
    echo "#================================================#"
    echo "#                                                #"
    echo "# Align vs whole genome (nc + mt), dedup, and    #"
    echo "# extract methylation                            #"
    echo "#                                                #"
    echo "##################################################"
    
     
    echo -ne "     1.1 Aligning... "
    
    if [ "${RAM}" -lt "16" ]; then cpus=1; else cpus=$( expr ${RAM} / 16 ); fi
    echo -ne "(${cpus} cores) "
    
    CMD="${bismark} ${b38_ref} -1 ${R1} -2 ${R2} --multicore ${cpus} ${subset_opt} \
        -o ${output_dir} --prefix b38 --path_to_bowtie ${bowtie_path} \
        --samtools_path ${samtools_path} --temp_dir ${tmp_dir} > \
        ${log_dir}/1_1_aln.stdout 2> ${log_dir}/1_1_aln.stderr"
    echo -e ${CMD} > ${log_dir}/1_1_aln.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/1_1_aln.cmd" --log ${log_dir}/1_1_aln.resources --interval 5 --include-children
    else
        bash ${log_dir}/1_1_aln.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo ;
        echo "ERROR IN STEP 1.1"
        exit 1
    else
        echo "DONE"
    fi
    
    
    echo -ne "     1.2 Merging bamfiles... "
    
    bamfile_list=""
    IFS=',' read -r -a array <<< "${R1}"
    for element in "${array[@]}"; do
        bamfile=$( basename ${element} )
        bamfile="${output_dir}/b38.${bamfile%.fastq.gz}_bismark_bt2_pe.bam"
        bamfile_list="${bamfile_list} ${bamfile}"
    done
    
    CMD="${samtools} cat -o ${output_dir}/b38_${sample_id}.bam ${bamfile_list} > \
        ${log_dir}/1_2_merge.stdout 2> \
        ${log_dir}/1_2_merge.stderr"
    echo -e ${CMD} > ${log_dir}/1_2_merge.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/1_2_merge.cmd" --log ${log_dir}/1_2_merge.resources --interval 1 --include-children
    else
        bash ${log_dir}/1_2_merge.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo ;
        echo "ERROR IN STEP 1.2"
        exit 1
    else
        echo "DONE"
    fi
    
    if [ -s "${output_dir}/b38_${sample_id}.bam" ]; then
        rm ${bamfile_list}
    fi
    
    
    echo -ne "     1.3 Dedupping alignment... "
    
    CMD="${deduplicate_bismark} --paired --samtools_path ${samtools_path} \
        --output_dir ${output_dir} ${output_dir}/b38_${sample_id}.bam > \
        ${log_dir}/1_3_dedup.stdout 2> \
        ${log_dir}/1_3_dedup.stderr"
    echo -e ${CMD} > ${log_dir}/1_3_dedup.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/1_3_dedup.cmd" --log ${log_dir}/1_3_dedup.resources --interval 5 --include-children
    else
        bash ${log_dir}/1_3_dedup.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo "ERROR IN STEP 1.3"
        exit 1
    else
        echo "DONE"
    fi
    
    if [ -s "${output_dir}/b38_${sample_id}.deduplicated.bam" ]; then
        mv ${output_dir}/b38_${sample_id}.deduplicated.bam ${output_dir}/b38_${sample_id}.dedup.bam
    fi
    
    if [ -s "${output_dir}/b38_${sample_id}.dedup.bam" ]; then
        rm ${output_dir}/b38_${sample_id}.bam
    fi
    
    echo -ne "     1.4 Extracting methylation... "
    
    cpus=${N_proc}
    
    CMD="${bismark_meth_extract} -p --comprehensive --output ${output_dir} \
        --multicore ${cpus} \
        --samtools_path ${samtools_path} \
        ${output_dir}/b38_${sample_id}.dedup.bam > \
        ${log_dir}/1_4_meth.stdout 2> \
        ${log_dir}/1_4_meth.stderr"
    echo -e ${CMD} > ${log_dir}/1_4_meth.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/1_4_meth.cmd" --log ${log_dir}/1_4_meth.resources --interval 5 --include-children
    else
        bash ${log_dir}/1_4_meth.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo "ERROR IN STEP 1.4"
        exit 1
    else
        echo "DONE"
    fi
    
    
    echo -ne "     1.5 Extracting MT and Lambda methylation... "
    
    CMD="cat ${output_dir}/CpG_context_b38_${sample_id}.dedup.txt | python3 ${ext_chr_srt} ${MT_chr_name} | pigz -nc > \
        ${output_dir}/CpG_context_chrM_${sample_id}.dedup.txt.gz && \
        cat ${output_dir}/CpG_context_b38_${sample_id}.dedup.txt | python3 ${ext_chr_srt} ${LAMBDA_chr_name} | pigz -nc > \
        ${output_dir}/CpG_context_LAMBDA_${sample_id}.dedup.txt.gz && \
        pigz -f ${output_dir}/CpG_context_b38_${sample_id}.dedup.txt \n\
    cat ${output_dir}/CHH_context_b38_${sample_id}.dedup.txt | python3 ${ext_chr_srt} ${MT_chr_name} | pigz -nc > \
        ${output_dir}/CHH_context_chrM_${sample_id}.dedup.txt.gz && \
        cat ${output_dir}/CHH_context_b38_${sample_id}.dedup.txt | python3 ${ext_chr_srt} ${LAMBDA_chr_name} | pigz -nc > \
        ${output_dir}/CHH_context_LAMBDA_${sample_id}.dedup.txt.gz && \
        pigz -f ${output_dir}/CHH_context_b38_${sample_id}.dedup.txt \n\
    cat ${output_dir}/CHG_context_b38_${sample_id}.dedup.txt | python3 ${ext_chr_srt} ${MT_chr_name} | pigz -nc > \
        ${output_dir}/CHG_context_chrM_${sample_id}.dedup.txt.gz && \
        cat ${output_dir}/CHG_context_b38_${sample_id}.dedup.txt | python3 ${ext_chr_srt} ${LAMBDA_chr_name} | pigz -nc > \
        ${output_dir}/CHG_context_LAMBDA_${sample_id}.dedup.txt.gz && \
        pigz -f ${output_dir}/CHG_context_b38_${sample_id}.dedup.txt \n\
        zcat ${output_dir}/C??_context_LAMBDA_${sample_id}.dedup.txt.gz | python3 ${meth_prop} > \
        ${output_dir}/Lambda_meth_prop_${sample_id}.txt"
    echo -e ${CMD} > ${log_dir}/1_5_extract_MT.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/1_5_extract_MT.cmd" --log ${log_dir}/1_5_extract_MT.resources --interval 5 --include-children
    else
        bash ${log_dir}/1_5_extract_MT.cmd
    fi
        
    if [ "$?" != "0" ]; then
        echo "ERROR IN STEP 1.5"
        exit 1
    else
        echo "DONE"
    fi
    
    echo
fi






if [[ $S2 == "true" ]]; then
    # Create TMP folder
    tmp_dir=${output_dir}/tmp_2
    mkdir -p ${tmp_dir}
    echo "##################################################"
    echo "#                   STRATEGY 2                   #"
    echo "#================================================#"
    echo "#                                                #"
    echo "# Align vs chrMT, dedup, and extract methylation #"
    echo "#                                                #"
    echo "##################################################"
    
     
    echo -ne "     2.1 Aligning... "
    
    if [ "${RAM}" -lt "16" ]; then cpus=1; else cpus=$( expr ${RAM} / 16 ); fi
    echo -ne "(${cpus} cores) "
    
    CMD="${bismark} ${mt_ref} -1 ${R1} -2 ${R2} --multicore ${cpus} ${subset_opt} \
        -o ${output_dir} --prefix MT --path_to_bowtie ${bowtie_path} \
        --samtools_path ${samtools_path} --temp_dir ${tmp_dir} > \
        ${log_dir}/2_1_aln.stdout 2> ${log_dir}/2_1_aln.stderr"
    echo -e ${CMD} > ${log_dir}/2_1_aln.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/2_1_aln.cmd" --log ${log_dir}/2_1_aln.resources --interval 5 --include-children
    else
        bash ${log_dir}/2_1_aln.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo ;
        echo "ERROR IN STEP 2.1"
        exit 1
    else
        echo "DONE"
    fi
    
    echo -ne "     2.2 Merging bamfiles... "
    
    cpus=`expr ${N_proc} / 2`
    
    bamfile_list=""
    IFS=',' read -r -a array <<< "${R1}"
    for element in "${array[@]}"; do
        bamfile=$( basename ${element} )
        bamfile="${output_dir}/MT.${bamfile%.fastq.gz}_bismark_bt2_pe.bam"
        bamfile_list="${bamfile_list} ${bamfile}"
    done
    
    CMD="${samtools} cat -o ${output_dir}/MT_${sample_id}.bam ${bamfile_list} > \
        ${log_dir}/2_2_merge.stdout 2> \
        ${log_dir}/2_2_merge.stderr"
    echo -e ${CMD} > ${log_dir}/2_2_merge.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/2_2_merge.cmd" --log ${log_dir}/2_2_merge.resources --interval 5 --include-children
    else
        bash ${log_dir}/2_2_merge.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo ;
        echo "ERROR IN STEP 2.2"
        exit 1
    else
        echo "DONE"
    fi
    
    if [ -s "${output_dir}/MT_${sample_id}.bam" ]; then
        rm ${bamfile_list}
    fi
    
    
    echo -ne "     2.3 Dedupping alignment... "
    
    CMD="${deduplicate_bismark} --paired --samtools_path ${samtools_path} \
        --output_dir ${output_dir} ${output_dir}/MT_${sample_id}.bam > \
        ${log_dir}/2_3_dedup.stdout 2> \
        ${log_dir}/2_3_dedup.stderr"
    echo -e ${CMD} > ${log_dir}/2_3_dedup.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/2_3_dedup.cmd" --log ${log_dir}/2_3_dedup.resources --interval 5 --include-children
    else
        bash ${log_dir}/2_3_dedup.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo "ERROR IN STEP 2.3"
        exit 1
    else
        echo "DONE"
    fi
    
    if [ -s "${output_dir}/MT_${sample_id}.deduplicated.bam" ]; then
        mv ${output_dir}/MT_${sample_id}.deduplicated.bam ${output_dir}/MT_${sample_id}.dedup.bam
    fi
    
    if [ -s "${output_dir}/MT_${sample_id}.dedup.bam" ]; then
        rm ${output_dir}/MT_${sample_id}.bam
    fi
    
    echo -ne "     2.4 Extracting methylation... "
    
    cpus=${N_proc}
    
    CMD="${bismark_meth_extract} -p --comprehensive --output ${output_dir} \
        --gzip --multicore ${cpus} \
        --samtools_path ${samtools_path} \
        ${output_dir}/MT_${sample_id}.dedup.bam > \
        ${log_dir}/2_4_meth.stdout 2> \
        ${log_dir}/2_4_meth.stderr"
    echo -e ${CMD} > ${log_dir}/2_4_meth.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/2_4_meth.cmd" --log ${log_dir}/2_4_meth.resources --interval 5 --include-children
    else
        bash ${log_dir}/2_4_meth.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo "ERROR IN STEP 2.4"
        exit 1
    else
        echo "DONE"
    fi
    
    echo
fi







if [[ $S3 == "true" ]]; then
    # Create TMP folder
    tmp_dir=${output_dir}/tmp_3
    mkdir -p ${tmp_dir}
    echo "##################################################"
    echo "#                   STRATEGY 3                   #"
    echo "#================================================#"
    echo "#                                                #"
    echo "# Align vs whole nuclear DNA, collect unmapped   #"
    echo "# and align them to the mtDNA                    #"
    echo "#                                                #"
    echo "##################################################"
    
     
     
    echo -ne "     3.1 Aligning vs nuclear DNA... "
    
    if [ "${RAM}" -lt "16" ]; then cpus=1; else cpus=$( expr ${RAM} / 16 ); fi
    echo -ne "(${cpus} cores) "
    
    CMD="${bismark} ${nuc_ref} -1 ${R1} -2 ${R2} --multicore ${cpus} ${subset_opt} \
        --un --ambiguous -o ${output_dir} --prefix b38_noMT --path_to_bowtie ${bowtie_path} \
        --samtools_path ${samtools_path} --temp_dir ${tmp_dir} > \
        ${log_dir}/3_1_aln.stdout 2> ${log_dir}/3_1_aln.stderr"
    echo -e ${CMD} > ${log_dir}/3_1_aln.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/3_1_aln.cmd" --log ${log_dir}/3_1_aln.resources --interval 5 --include-children
    else
        bash ${log_dir}/3_1_aln.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo ;
        echo "ERROR IN STEP 3.1"
        exit 1
    else
        echo "DONE"
    fi
    
    echo -ne "     3.2 Merging unaligned reads... "
    
    unmapped_list_r1=""
    unmapped_list_r2=""
    IFS=',' read -r -a array <<< "${R1}"
    for element in "${array[@]}"; do
        uns=$( basename ${element} )
        uns="${output_dir}/b38_noMT.${uns}_unmapped_reads_1.fq.gz"
        unmapped_list_r1="${unmapped_list_r1} ${uns}"
    done
    IFS=',' read -r -a array <<< "${R2}"
    for element in "${array[@]}"; do
        uns=$( basename ${element} )
        uns="${output_dir}/b38_noMT.${uns}_unmapped_reads_2.fq.gz"
        unmapped_list_r2="${unmapped_list_r2} ${uns}"
    done
    
    CMD="cat ${unmapped_list_r1} > ${output_dir}/b38_noMT_${sample_id}_unmapped.R1.fastq.gz && \
        rm ${unmapped_list_r1} \n\
        cat ${unmapped_list_r2} > ${output_dir}/b38_noMT_${sample_id}_unmapped.R2.fastq.gz && \
        rm ${unmapped_list_r2} \n\
        rm ${output_dir}/b38_noMT.*_ambiguous_reads_?.fq.gz"
    echo -e ${CMD} > ${log_dir}/3_2_merge_unmapped.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/3_2_merge_unmapped.cmd" --log ${log_dir}/3_2_merge_unmapped.resources --interval 5 --include-children
    else
        bash ${log_dir}/3_2_merge_unmapped.cmd
    fi
     
    if [ "$?" != "0" ]; then
        echo ;
        echo "ERROR IN STEP 3.2"
        exit 1
    else
        echo "DONE"
    fi
    
    
    
    
    echo -ne "     3.3 Deleting nuclear bamfiles... "
    
    bamfile_list=""
    IFS=',' read -r -a array <<< "${R1}"
    for element in "${array[@]}"; do
        bamfile=$( basename ${element} )
        bamfile="${output_dir}/b38_noMT.${bamfile%.fastq.gz}_bismark_bt2_pe.bam"
        bamfile_list="${bamfile_list} ${bamfile}"
    done
    
    CMD="rm ${bamfile_list}"
    echo ${CMD} > ${log_dir}/3_3_delete.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/3_3_delete.cmd" --log ${log_dir}/3_3_delete.resources --interval 5 --include-children
    else
        bash ${log_dir}/3_3_delete.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo ;
        echo "ERROR IN STEP 3.3"
        exit 1
    else
        echo "DONE"
    fi
    
    
    
     
    echo -ne "     3.4 Re-aligning unmapped reads to mtDNA... "
    
    if [ "${RAM}" -lt "16" ]; then cpus=1; else cpus=$( expr ${RAM} / 16 ); fi
    echo -ne "(${cpus} cores) "
    
    CMD="(${bismark} ${mt_ref} -1 ${output_dir}/b38_noMT_${sample_id}_unmapped.R1.fastq.gz \
        -2 ${output_dir}/b38_noMT_${sample_id}_unmapped.R2.fastq.gz --multicore ${cpus} \
        -o ${output_dir} --prefix umap_to_MT --path_to_bowtie ${bowtie_path} \
        --samtools_path ${samtools_path} --temp_dir ${tmp_dir} && \
        mv ${output_dir}/umap_to_MT.b38_noMT_${sample_id}_unmapped.R1_bismark_bt2_pe.bam ${output_dir}/umap_to_MT_${sample_id}.bam && \
        mv ${output_dir}/umap_to_MT.b38_noMT_${sample_id}_unmapped.R1_bismark_bt2_PE_report.txt ${output_dir}/umap_to_MT_${sample_id}_report.txt && \
        rm ${output_dir}/b38_noMT_${sample_id}_unmapped.R?.fastq.gz) > \
        ${log_dir}/3_4_realign_to_mt.stdout 2> \
        ${log_dir}/3_4_realign_to_mt.stderr"
    echo ${CMD} > ${log_dir}/3_4_realign_to_mt.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/3_4_realign_to_mt.cmd" --log ${log_dir}/3_4_realign_to_mt.resources --interval 5 --include-children
    else
        bash ${log_dir}/3_4_realign_to_mt.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo "ERROR IN STEP 3.4"
        exit 1
    else
        echo "DONE"
    fi
    
    
    
    echo -ne "     3.5 Dedupping alignments... "
    
    CMD="${deduplicate_bismark} --paired --samtools_path ${samtools_path} \
        --output_dir ${output_dir} ${output_dir}/umap_to_MT_${sample_id}.bam > \
        ${log_dir}/3_5_dedup.stdout 2> \
        ${log_dir}/3_5_dedup.stderr"
    echo -e ${CMD} > ${log_dir}/3_5_dedup.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/3_5_dedup.cmd" --log ${log_dir}/3_5_dedup.resources --interval 5 --include-children
    else
        bash ${log_dir}/3_5_dedup.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo "ERROR IN STEP 3.5"
        exit 1
    else
        echo "DONE"
    fi
    
    if [ -s "${output_dir}/umap_to_MT_${sample_id}.deduplicated.bam" ]; then
        mv ${output_dir}/umap_to_MT_${sample_id}.deduplicated.bam ${output_dir}/umap_to_MT_${sample_id}.dedup.bam
    fi
    
    if [ -s "${output_dir}/umap_to_MT_${sample_id}.dedup.bam" ]; then
        rm ${output_dir}/umap_to_MT_${sample_id}.bam
    fi
    
    
    
    echo -ne "     3.6 Extracting methylation in MT..."
    
    cpus=${N_proc}
    
    CMD="${bismark_meth_extract} -p --comprehensive --output ${output_dir} \
        --gzip --multicore ${cpus} \
        --samtools_path ${samtools_path} \
        ${output_dir}/umap_to_MT_${sample_id}.dedup.bam > \
        ${log_dir}/3_6_meth.stdout 2> \
        ${log_dir}/3_6_meth.stderr"
    echo -e ${CMD} > ${log_dir}/3_6_meth.cmd
    if $MONITOR; then
        echo -ne "\n     "
        ${psrecord} "bash ${log_dir}/3_6_meth.cmd" --log ${log_dir}/3_6_meth.resources --interval 5 --include-children
    else
        bash ${log_dir}/3_6_meth.cmd
    fi
    
    if [ "$?" != "0" ]; then
        echo "ERROR IN STEP 3.6"
        exit 1
    else
        echo "DONE"
    fi
    
    
    echo
    
    echo "Analysis finished: ${output_dir}"
    size_used=$( du -hs ${output_dir} | cut -f 1 )
    echo "Amount of data to disk: ${size_used}"
    
    end_date=$( date )
    echo "START: ${start_date}"
    echo "END:   ${end_date}"
fi




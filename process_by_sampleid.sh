#!/bin/bash

# set -e
# trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# trap 'if [ "$?" -ne "0" ]; then echo "\"${last_command}\" command filed with exit code different from 0"; fi' EXIT

###### FUNCTIONS

usage()
{
    echo ""
    echo "USAGE:"
    echo "$0 [OPTIONS] -s <SAMPLE_ID> -i <STORAGE_ACCT_FASTQ>/<FILESHARE_FASTQ> -o <STORAGE_ACCT_RESULTS>/<FILESHARE_RESULTS_PREFIX>"
    echo "  - SAMPLE_ID: e.g. SL284409"
    echo "  - STORAGE_ACCT_FASTQ: e.g. forneurosysmedutvcompute"
    echo "  - FILESHARE_FASTQ: Name of the fileshare, e.g. wgbsfqdata"
    echo "  - STORAGE_ACCT_RESULTS: e.g. forneurosysmedutvcompute"
    echo ""
    echo " EXTRA OPTIONS"
    echo "   -f --force"
    echo "          Overwrite previous results, if any."
    echo "   -m --monitor"
    echo "          Monitor RAM and CPU usage."
    echo "   -u --subset INTEGER"
    echo "          Subset reads to test pipeline (to INTEGER)."
    echo "   --strategies STRATEGIES"
    echo "          Only run selected strategies. Possible choices of STRATEGIES: "1", "2", "3""
    echo "          or any combination of them, comma-separated (e.g. "1,2,3", "2,3")."
    echo "   --shrink-fileshare"
    echo "          When finished, reduce quota of fileshare to be only 10G more than the total"
    echo "          space needed for the results. This option is not to be used when other tasks"
    echo "          are running on the same fileshare (e.g. when running different strategies"
    echo "          concomitantly."
    echo "   --input-samba-credentials FILE"
    echo "          File with the samba credentials to mount input fileshare (fastqs)."
    echo "   --output-samba-credentials FILE"
    echo "          File with the samba credentials to mount output fileshare (results)."
    echo "There should be files in the /etc/smbcredentials/ folder named <STORAGE_ACCT_FASTQ>.cred and <STORAGE_ACCT_RESULTS>.cred (the storage account can be the same for both input fastqs and results, then only one file is needed). This/these file/s contain the username=... and the password=... lines with the storage account name and the key."
    echo ""
    echo "For the results, a new fileshare will be created for the sample analysed, whose name will be a concatenation of the form <FILESHARE_RESULTS_PREFIX>-<SAMPLE_ID>. If the fileshare name exists and is not empty, the program will fail."
    echo ""
    echo "EXAMPLE: $0 -s SL284405 -i forneurosysmedutvcompute/wgbsfqdata -o forneurosysmedutvcompute/prefix"
}

###### PARSE CMDLINE

sample_id=
storageAccountFastq=
shareNameFastq=
storageAccountResults=
shareNameResults=
FORCE=0
MONITOR=0
SUBSET=0
STRATEGIES="1,2,3"
SHRINK=0

smbCredFastq=
smbCredResults=

while [ "$1" != "" ]; do
    case $1 in
        -s | --sample-id )               shift
                                         sample_id=$1
                                         ;;
        -i | --input-storage-account )   shift
                                         storageAccountFastq=${1%/*}
                                         shareNameFastq=${1#*/}
                                         ;;
        -o | --output-storage-account )  shift
                                         storageAccountResults=${1%/*}
                                         shareNameResults=${1#*/}
                                         ;;
        --input-samba-credentials )      shift
                                         smbCredFastq=$1
                                         ;;
        --output-samba-credentials )     shift
                                         smbCredResults=$1
                                         ;;
        -f | --force )                   FORCE=1
                                         ;;
        -m | --monitor )                 MONITOR=1
                                         ;;
        -u | --subset )                  shift
                                         SUBSET=$1
                                         ;;
        --strategies )                   shift
                                         STRATEGIES=$1
                                         ;;
        --shrink-fileshare )             SHRINK=1
                                         ;;
        -h | --help )                    usage
                                         exit
                                         ;;
        * )                              usage
                                         exit 1
    esac
    shift
done

if [[ "$sample_id" == "" ]]; then
    echo "[ERROR] -s option is mandatory."
    exit 2
fi
if [[ "$storageAccountFastq" == "" ]]; then
    echo "[ERROR] -i option is mandatory, e.g. forneurosysmedutvcompute/wgbsfqdata"
    exit 2
fi
if [[ "$shareNameFastq" == "" ]]; then
    echo "[ERROR] -i option is mandatory, e.g. forneurosysmedutvcompute/wgbsfqdata"
    exit 2
fi
if [[ "$storageAccountResults" == "" ]]; then
    echo "[ERROR] -o option is mandatory, e.g. forneurosysmedutvcompute/prefix"
    exit 2
fi
if [[ "$shareNameResults" == "" ]]; then
    echo "[ERROR] -o option is mandatory, e.g. forneurosysmedutvcompute/prefix"
    exit 2
fi

if [[ "$SUBSET" == "" ]]; then
    echo "[ERROR] -u option needs an integer, e.g. -u 10000"
    exit 2
fi
re='^[0-9]+$'
if ! [[ $SUBSET =~ $re ]]; then
       echo "[ERROR] -u option needs and integer, e.g. -u 100000"
       exit 2
fi

shareNameResults=$( echo ${shareNameResults}-${sample_id} | tr '[:upper:]' '[:lower:]' )

if [[ "$smbCredFastq" == "" ]]; then
    smbCredFastq=/etc/smbcredentials/${storageAccountFastq}.cred
    echo "[WARNING] Using default samba credentials file path: $smbCredFastq"
fi
if [[ "$smbCredResults" == "" ]]; then
    smbCredResults=/etc/smbcredentials/${storageAccountResults}.cred
    echo "[WARNING] Using default samba credentials file path: $smbCredResults"
fi


echo
echo "INPUT / VARIABLES"
echo "================="
echo "- sample_id: ${sample_id}"
echo "- storageAccountFastq: ${storageAccountFastq}"
echo "- shareNameFastq: ${shareNameFastq}"
echo "- storageAccountResults: ${storageAccountResults}"
echo "- shareNameResults: ${shareNameResults}"
echo "- smbCredFastq: ${smbCredFastq}"
echo "- smbCredResults: ${smbCredResults}"
echo "- FORCE: $FORCE"
echo "- MONITOR: $MONITOR"
echo "- SUBSET: $SUBSET"
echo "- STRATEGIES: ${STRATEGIES}"
echo "- SHRINK: ${SHRINK}"
echo

echo "ACTUALLY DOING STUFF"
echo "===================="
# Check credential files exist
echo ">> Getting keys (needs sudo)..."
if [[ ! -s $smbCredFastq ]]; then
    echo "   [ERROR] File \"$smbCredFastq\" does not exist"
    exit 2
fi
if [[ ! -s $smbCredResults ]]; then
    echo "   [ERROR] File \"$smbCredResults\" does not exist"
    exit 2
fi

storageAccountKeyFastq=$( sudo grep '^password=' $smbCredFastq | sed 's/^password=//' )
storageAccountKeyResults=$( sudo grep '^password=' $smbCredResults | sed 's/^password=//' )

if [[ $storageAccountKeyFastq == "" || $storageAccountKeyResults == "" ]]; then
    echo "   [ERROR] Samba credentials need to have \"username\" and \"passoword\" fields"
    exit 2
fi

echo "   [DONE]"


echo ">> Preparing input file share... "
# Check that fastq file share exists
exists=$( az storage share exists --account-name ${storageAccountFastq} --account-key ${storageAccountKeyFastq} --name ${shareNameFastq} --output tsv )
if [[ $exists != "True" ]]; then
    echo "   [ERROR] File share \"${shareNameFastq}\" in storage account \"${storageAccountFastq}\" does not exist"
    exit 2
fi

fastq_folder="/fileshares/${storageAccountFastq}/${shareNameFastq}"

sudo mkdir -p ${fastq_folder}

# Check if is mounted
isMounted=$( { grep -cs "${fastq_folder}" /proc/mounts || true; } )

if [[ $isMounted != 0 ]]; then
    echo "  *Already mounted, remounting..."
    sudo umount ${fastq_folder}
fi

# Mount it
sudo mount -t cifs //${storageAccountFastq}.file.core.windows.net/${shareNameFastq} \
    ${fastq_folder} \
    -o vers=3.0,credentials=/etc/smbcredentials/${storageAccountFastq}.cred,dir_mode=0755,file_mode=0644,serverino,gid=1000,uid=1000,forceuid,forcegid

if [[ $? != 0 ]]; then
    echo "   [ERROR] Could not mount file share for FASTQ files"
    exit 2
fi
echo "   [DONE]"

echo "  *Input FASTQ file share mounted in \"${fastq_folder}\""

echo ">> Preparing output file share... "
# Check that results fileshare does NOT exist or issue warning and wait
exists=$( az storage share exists --account-name ${storageAccountResults} --account-key ${storageAccountKeyResults} --name "${shareNameResults}" --output tsv )
if [[ $exists == "False" ]]; then
    az storage share create \
        --account-name ${storageAccountResults} \
        --account-key ${storageAccountKeyResults} \
        --name "${shareNameResults}" \
        --quota 9216 \
        --output none
else
    if [[ $FORCE == 0 ]]; then
        echo "   [ERROR] File share \"${shareNameResults}\" in \"${storageAccountResults}\""
        echo "           ALREADY EXISTS!"
        echo "           If you don't mind potentially overwriting, use -f option"
        exit 2
    else
        quota=$( az storage share show --account-name ${storageAccountResults} --account-key ${storageAccountKeyResults} --name "${shareNameResults}" --output json --query properties.quota )
        if [[ $quota < 9216 ]]; then
            echo "   [WARNING] File share \"${shareNameResults}\" in \"${storageAccountResults}\""
            echo "             has a quota smaller than 9216G, resizing to 9216..."
            az storage share update --account-name ${storageAccountResults} --account-key ${storageAccountKeyResults} --name "${shareNameResults}" --quota 9216
        fi
    fi
fi


results_folder="/fileshares/${storageAccountResults}/${shareNameResults}"


sudo mkdir -p ${results_folder}

# Check if is mounted
isMounted=$( { grep -cs "${results_folder}" /proc/mounts || true; } )

if [[ $isMounted != 0 ]]; then
    echo "  *Already mounted, remounting..."
    sudo umount ${results_folder}
fi

USR_ID=$( id -u )
GRP_ID=$( id -g )
sudo mount -t cifs //${storageAccountResults}.file.core.windows.net/${shareNameResults} \
    ${results_folder} \
    -o vers=3.0,credentials=/etc/smbcredentials/${storageAccountResults}.cred,dir_mode=0777,file_mode=0666,serverino,gid=${USR_ID},uid=${GRP_ID},forceuid,forcegid

if [[ $? != 0 ]]; then
    echo "   [ERROR] Could not mount file share for RESULTS"
    exit 2
else
    cd ${results_folder}
fi

echo "   [DONE]"

echo "   *Results file share mounted in \"${results_folder}\""



# Get fastq file names...
echo ">> Getting FASTQ filenames... "

all_files=($(find ${fastq_folder}/*${sample_id}*.fastq.gz))
num_files=${#all_files[@]}
if [[ $num_files -eq 0 ]]; then
    echo "   [ERROR] No fastq files retrieved"
    exit 3
fi

echo "   [DONE]"
echo "  *Number of FASTQ files found: ${num_files}"

declare -a R1
declare -a R2

# get R1 files and sort them
for file in "${all_files[@]}"; do
    IFS='.' read -r -a array <<< "${file}"
    if [[ ${array[1]} == "R1" ]]; then
        echo "    - R1: ${file}"
        R1+=(${file})
    else
        echo "    - R2: ${file}"
        R2+=(${file})
    fi
done


# RUNNIN' THE PIPELINE
opts=" -o ${results_folder} "
if [[ $FORCE != 0 ]]; then
    opts="${opts}-f "
fi
if [[ $MONITOR != 0 ]]; then
    opts="${opts}-m "
fi
if [[ $SUBSET != 0 ]]; then
    opts="${opts}-u ${SUBSET} "
fi
opts="${opts}-s $STRATEGIES "

PIPELINE_DIR=`dirname $0`
PIPELINE_CMD="bash ${PIPELINE_DIR}/wgbs-pipeline.sh${opts}${R1[*]} ${R2[*]}"
echo ">> RUNNING THE PIPELINE..."
echo "   CMD = \"${PIPELINE_CMD}\""
${PIPELINE_CMD}
echo
echo "**************FINISHED**************"
echo

# Resize storage file share (CAREFUL IF OTHER TASKS NEED THE SPACE!)
if [[ $SHRINK > 0 ]]; then
    echo ">> Shrinking the fileshare..."
    # Get space used by files in share in GB
    sleep 30
    size1=$( az storage share stats --name ${shareNameResults} --account-name ${storageAccountResults} --account-key ${storageAccountKeyResults} )
    size2=$( du -sh -BG ${results_folder} | cut -f 1 | tr -d 'G' )
    size=$(( size1 > size2 ? size1 + 10 : size2 + 10))
    
    echo " Output files use around $(( size - 10 )) G"
    echo " Resizing fileshare quota to $size G..."
    
    az storage share update --name ${shareNameResults} --account-name ${storageAccountResults} --account-key ${storageAccountKeyResults} --quota ${size}
else
    echo "   * Fileshare in storage account left with original quota"
fi

echo 
cd ~
echo " Unmounting FASTQ folder..."
sudo umount ${fastq_folder}
echo " Unmounting results folder..."
sudo umount ${results_folder}
echo "   [DONE]"



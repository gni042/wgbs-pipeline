import sys

if len(sys.argv) != 2:
    sys.stderr.write("USAGE: ext_chr_srt.py CHR_NAME\n    CHR_NAME: chromosome name to extract (e.g. \"chrM\")\n")
    sys.exit(1)

chromosome = sys.argv[1]

for line in sys.stdin:
    if line.startswith("Bismark"):
        sys.stdout.write(line)
        continue
    if line.split("\t")[2] == chromosome:
        sys.stdout.write(line)

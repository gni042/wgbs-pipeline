#!/bin/bash

# Programs
samtools="/usr/local/bin/samtools-1.10"
bismark_index="/usr/local/bin/bismark_genome_preparation-0.22.3"
bowtie_path="/opt/bowtie2-2.4.1/"

# Check arguments
if [[ $# -lt 1 ]]; then
    echo "ERROR: Provide output folder for the references"
    exit 1
fi

# Directories where the references will be stored
mkdir -p $1
output_dir=$( realpath $1 )
echo "Reference files will be stored in \"${output_dir}\""
# === #
grch38_dir=${output_dir}/GRCh38
mkdir -p ${grch38_dir}
grch38_no_mt_dir=${output_dir}/GRCh38_no_mt
mkdir -p ${grch38_no_mt_dir}
mt_dir=${output_dir}/chrM
mkdir -p ${mt_dir}
lambda_dir=${output_dir}/lambda
mkdir -p ${lambda_dir}

# Fasta filenames
grch38_fasta=${grch38_dir}/GRCh38_no_alt_with_lambda.fasta
grch38_no_mt_fasta=${grch38_no_mt_dir}/GRCh38_no_alt_no_mt.fasta
mt_fasta=${mt_dir}/chrM.fasta
lambda_fasta=${lambda_dir}/lambda.fasta



# URL with the files from ENCODE
GRCH38_url="https://www.encodeproject.org/files/GRCh38_no_alt_analysis_set_GCA_000001405.15/@@download/GRCh38_no_alt_analysis_set_GCA_000001405.15.fasta.gz"
LAMBDA_url="https://www.encodeproject.org/files/lambda.fa/@@download/lambda.fa.fasta.gz"



# Get number of processors in the machine
N_proc=`expr $( grep -c processor /proc/cpuinfo ) / 2`

# 1) Obtain GRCh38 human genome from ENCODE
echo "1/8 - Downloading GRCh38 reference..."
wget --show-progress -O ${grch38_fasta}.gz ${GRCH38_url}
echo "      Decompressing GRCh38 reference..."
gunzip -f ${grch38_fasta}.gz



# 2) Obtain Lambda genome
echo "2/8 - Downloading Lambda genome..."
wget --show-progress -O ${lambda_fasta}.gz ${LAMBDA_url}
echo "      Decompressing Lambda reference..."
gunzip -f ${lambda_fasta}.gz



# 3) Generate chrM genome fomr GRCh38
echo "3/8 - Extracting mtDNA from GRCh38 reference..."
${samtools} faidx ${grch38_fasta} chrM > ${mt_fasta}



# 4) Generate GRCh38 without chrM
echo "4/8 - Removing mtDNA from GRCh38 reference..."
all_chr=$( grep -v '^chrM' ${grch38_fasta}.fai | cut -f 1 )
${samtools} faidx ${grch38_fasta} ${all_chr} > ${grch38_no_mt_fasta}



# 5) Add Lambda to GRCh38
echo "5/8 - Adding Lambda genome to GRCh38 reference..."
cat ${lambda_fasta} >> ${grch38_fasta}
rm ${grch38_fasta}.fai



# 6) Generate index for GRCh38
echo "6/8 - Generating indices for GRCh38..."
${bismark_index} --parallel ${N_proc} --path_to_aligner ${bowtie_path} ${grch38_dir}



# 7) Generate index for GRCh38 without MT
echo "7/8 - Generating indices for GRCh38 without mtDNA..."
${bismark_index} --parallel ${N_proc} --path_to_aligner ${bowtie_path} ${grch38_no_mt_dir}



# 8) Generate index for chrM
echo "8/8 - Generating indices for chrM..."
${bismark_index} --parallel ${N_proc} --path_to_aligner ${bowtie_path} ${mt_dir}





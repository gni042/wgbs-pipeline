import sys

total = 0
methylated = 0
for line in sys.stdin:
    if line.startswith("Bismark"):
        continue
    if line.split("\t")[1] == '+': # Methylated
        methylated += 1
    total += 1

sys.stdout.write("Methylation proportion: {:.2f}% ({}/{})\n".format(100*(methylated/total),methylated,total))
        

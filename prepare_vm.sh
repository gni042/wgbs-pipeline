#!/bin/bash

echo "#### Updating Ubuntu dist..."
sudo apt update && sudo apt -y upgrade

echo "#### Installing some Ubuntu packages..."
sudo apt -y install unzip build-essential libncurses5-dev zlib1g-dev libbz2-dev liblzma-dev p7zip-full pigz
sudo apt install -y neovim python3-psutil default-jre

echo "#### Installing samtools 1.10..."
cd ~
wget https://github.com/samtools/samtools/releases/download/1.10/samtools-1.10.tar.bz2
tar -xvjf samtools-1.10.tar.bz2
rm samtools-1.10.tar.bz2
cd samtools-1.10
./configure
make
sudo mkdir /opt/samtools-1.10
sudo cp ./samtools /opt/samtools-1.10/samtools
sudo ln -s /opt/samtools-1.10/samtools /usr/local/bin/samtools-1.10

echo "#### Installing bowtie2-2.4.1..."
cd ~
wget https://github.com/BenLangmead/bowtie2/releases/download/v2.4.1/bowtie2-2.4.1-linux-x86_64.zip
unzip bowtie2-2.4.1-linux-x86_64.zip
rm bowtie2-2.4.1-linux-x86_64.zip
sudo mkdir /opt/bowtie2-2.4.1
sudo cp -r bowtie2-2.4.1-linux-x86_64/* /opt/bowtie2-2.4.1/
sudo ln -s /opt/bowtie2-2.4.1/bowtie2 /usr/local/bin/bowtie2-2.4.1

echo "#### Installing Bismark 0.22.3..."
wget https://github.com/FelixKrueger/Bismark/archive/0.22.3.tar.gz
tar -xvzf 0.22.3.tar.gz
rm 0.22.3.tar.gz
sudo mkdir /opt/Bismark-0.22.3
sudo cp -r Bismark-0.22.3/* /opt/Bismark-0.22.3/
sudo ln -s /opt/Bismark-0.22.3/bismark_genome_preparation /usr/local/bin/bismark_genome_preparation-0.22.3
sudo ln -s /opt/Bismark-0.22.3/deduplicate_bismark /usr/local/bin/deduplicate_bismark-0.22.3
sudo ln -s /opt/Bismark-0.22.3/coverage2cytosine /usr/local/bin/coverage2cytosine-0.22.3
sudo ln -s /opt/Bismark-0.22.3/bismark_methylation_extractor /usr/local/bin/bismark_methylation_extractor-0.22.3
sudo ln -s /opt/Bismark-0.22.3/bismark2summary /usr/local/bin/bismark2summary-0.22.3
sudo ln -s /opt/Bismark-0.22.3/bismark2report /usr/local/bin/bismark2report-0.22.3
sudo ln -s /opt/Bismark-0.22.3/bismark2bedGraph /usr/local/bin/bismark2bedGraph-0.22.3
sudo ln -s /opt/Bismark-0.22.3/bismark /usr/local/bin/bismark-0.22.3
sudo ln -s /opt/Bismark-0.22.3/bam2nuc /usr/local/bin/bam2nuc-0.22.3

echo "#### Installing FastQC"
cd ~
wget https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.9.zip
unzip fastqc_v0.11.9.zip 
rm fastqc_v0.11.9.zip 
sudo mkdir /opt/fastqc-0.11.9
sudo cp -r FastQC/* /opt/fastqc-0.11.9/
sudo chmod 777 /opt/fastqc-0.11.9/fastqc
sudo ln -s /opt/fastqc-0.11.9/fastqc /usr/local/bin/fastqc-0.11.9
sudo ln -s /usr/local/bin/fastqc-0.11.9 /usr/local/bin/fastqc
rm -r ./FastQC
cd ~


echo "#### Installing Trimmomatic..."
cd ~
wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.39.zip
unzip Trimmomatic-0.39.zip
rm Trimmomatic-0.39.zip
sudo cp -r Trimmomatic-0.39 /opt/
rm -r Trimmomatic-0.39
echo 'java -jar /opt/Trimmomatic-0.39/trimmomatic-0.39.jar $*' | sudo tee /usr/local/bin/trimmomatic-0.39
sudo chmod 775 /usr/local/bin/trimmomatic-0.39
sudo ln -s /usr/local/bin/trimmomatic-0.39 /usr/local/bin/trimmomatic





echo "Installing R..."
sudo apt -y install r-base
sudo apt -y install libcurl4-openssl-dev libxml2-dev

echo "Installing psrecord for monitoring"
sudo apt -y install python-pip
pip install psrecord

echo "Fetching config files"
cd ~
git clone https://git.app.uib.no/gni042/configs.git
cd configs
bash INSTALL.sh

echo "export PATH=~/.local/bin/:$PATH" >> ~/.bashrc

#R packages
#----------
#
#- methylKit
#  install.packages("BiocManager")
#  BiocManager::install("methylKit")
#  BiocManager::install(c('GenomicRanges', 'GenomeInfoDb', 'emdbook', 'Rsamtools', 'fastseg', 'rtracklayer'))


# echo "Installing extra software..."
# sudo apt install openjdk-11-jdk
# mkdir /data/bin
# cd /data/bin
# git clone "https://github.com/lindenb/jvarkit.git"
# cd jvarkit
# ./gradlew samviewwithmate
# sudo apt install python3-pip
# sudo pip3 install cython
# sudo pip3 install pysam


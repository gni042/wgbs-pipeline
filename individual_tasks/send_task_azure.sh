#!/bin/bash

usage()
{
    echo ""
    echo "USAGE:"
    echo "$0 [OPTIONS] -s SAMPLEID -t TASKID -a AZURETASKNAME -j JOBID"
    echo ""
    echo "OPTIONS"
    echo "   -s SAMPLEID"
    echo "          [REQUIRED] Sample id (e.g. SL284406)."
    echo "   -t TASKID"
    echo "          [REQUIRED] ID of the task (e.g. 1_2)."
    echo "   -a AZURETASKNAME"
    echo "          [REQUIRED] Name for the task for Azure batch."
    echo "   -j JOBID"
    echo "          [REQUIRED] Azure batch job id where the task is being sent."
    echo "   -u INTEGER"
    echo "          Subset reads to test pipeline (to INTEGER)."
    echo "   -c INTEGER"
    echo "          Number of cores to use (INTEGER)."
    echo "   -m"
    echo "          Monitor RAM/CPU."
    echo "   -h"
    echo "          Print (this) help message."
    echo ""
}

# # NEEDS LOGGING IN INTO BATCH ACCOUNT !!!
# az batch account login --name forneruosysmedutvbatch \
#     --resource-group for-neuro-sysmed-utv-compute
# az batch pool create --id wgbs-v5-pool --vm-size standard_d8_v3 \
#     --node-agent-sku-id "batch.node.ubuntu 18.04" \
#     --image /subscriptions/5a9e26a0-6897-44d6-963e-fae2a2061f27/resourceGroups/FOR-NEURO-SYSMED-UTV-COMPUTE/providers/Microsoft.Compute/galleries/neurosysmed.img.gallery/images/wgbs-vs-imgdef/versions/1.5.0 \
#     --target-dedicated-nodes 20
#az batch job create --id wgbs-strategy-2 --pool-id wgbs-v5-pool

#### PARSE CMD LINE

task_id=
azure_task_id=
sample_id=
azure_job_id=
subset=0
cores=1
MONITOR=0

while getopts ":s:t:a:j:u:c:h" opt; do
    case ${opt} in 
        s)
            sample_id=$OPTARG
            ;;
        t)
            task_id=$OPTARG
            ;;
        a)
            azure_task_id=$OPTARG
            ;;
        j)
            azure_job_id=$OPTARG
            ;;
        u)
            subset=$OPTARG
            ;;
        m)
            MONITOR=1
            ;;
        c)
            cores=$OPTARG
            ;;
        h)
            usage
            exit 0
            ;;
        \?)
            echo "[ERROR] Invalid option: -$OPTARG" >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
        :)
            echo "[ERROR] Option -$OPTARG requires an argument." >&2
            echo "        (try $0 -h for help)"
            exit 1
            ;;
    esac
done

if [[ "$task_id" == "" ]]; then
    echo "[ERROR] Task id (-t option) is required."
    exit 1
fi
if [[ "$sample_id" == "" ]]; then
    echo "[ERROR] Sample id (-i option) is required."
    exit 1
fi
if [[ "$azure_task_id" == "" ]]; then
    echo "[ERROR] Azure task id (-a option) is required."
    exit 1
fi
if [[ "$azure_job_id" == "" ]]; then
    echo "[ERROR] Azure job id (-j option) is required."
    exit 1
fi

subset_opt=""
if [[ $subset > 0 ]]; then
    subset_opt=" -u ${subset}"
fi
if [[ $cores > 1 ]]; then
    subset_opt="${subset_opt} -c ${cores}"
fi
if [[ $MONITOR == 1 ]]; then
    subset_opt="${subset_opt} -m"
fi

CMD_LINE="/bin/bash /fileshares/forneurosysmedutvcompute/wgbs-resources/wgbs-pipeline/individual_tasks/task.sh -s ${sample_id} -t ${task_id} -f${subset_opt} -i forneurosysmedutvcompute/wgbsfqdata -o forneurosysmedutvcompute/bs"
#CMD_LINE="/bin/bash /fileshares/forneurosysmedutvcompute/wgbs-resources/wgbs-pipeline/individual_tasks/task.sh -s ${sample_id} -t ${task_id} -f${subset_opt} -i forneurosysmedutvcompute/wgbsfqdata -o forneurosysmedutvcompute/pref"

# CREATE JSON
tmp_json=$(mktemp /tmp/task.XXXXXX)
echo "{" > $tmp_json
echo "  \"id\": \"${azure_task_id}\"," >> $tmp_json
echo "  \"commandLine\": \"${CMD_LINE}\"," >> $tmp_json
echo "  \"userIdentity\": {" >> $tmp_json
echo "    \"autoUser\": {" >> $tmp_json
echo "      \"elevationLevel\": \"admin\"," >> $tmp_json
echo "      \"scope\": \"pool\"" >> $tmp_json
echo "    }," >> $tmp_json
echo "    \"userName\": null" >> $tmp_json
echo "  }" >> $tmp_json
echo "}" >> $tmp_json

az batch task create --json-file ${tmp_json} --job-id ${azure_job_id}





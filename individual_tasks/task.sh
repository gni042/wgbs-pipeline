#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" > /dev/null 2>&1 ; pwd -P )"

## PROGRAMS
bowtie_path="/opt/bowtie2-2.4.1"

samtools_path="/opt/samtools-1.10"
samtools="${samtools_path}/samtools"

bismark_path="/opt/Bismark-0.22.3"
bismark="${bismark_path}/bismark"
deduplicate_bismark="${bismark_path}/deduplicate_bismark"
bismark_meth_extract="${bismark_path}/bismark_methylation_extractor"

ext_chr_srt="${SCRIPTPATH}/../apps/ext_chr_srt.py"
meth_prop="${SCRIPTPATH}/../apps/methylation_proportion.py"

usage()
{
    echo ""
    echo "USAGE:"
    echo "$0 [OPTIONS] -s <SAMPLE_ID> -t <TASK_ID> -i <STORAGE_ACCT_FASTQ>/<FILESHARE_FASTQ> -o <STORAGE_ACCT_RESULTS>/<FILESHARE_RESULTS_PREFIX>"
    echo "  - SAMPLE_ID: e.g. SL284409"
    echo "  - TASK_ID: e.g. 1_1"
    echo "  - STORAGE_ACCT_FASTQ: e.g. forneurosysmedutvcompute"
    echo "  - FILESHARE_FASTQ: Name of the fileshare, e.g. wgbsfqdata"
    echo "  - STORAGE_ACCT_RESULTS: e.g. forneurosysmedutvcompute"
    echo ""
    echo " EXTRA OPTIONS"
    echo "   -f --force"
    echo "          Overwrite previous results, if any."
    echo "   -u --subset INTEGER"
    echo "          Subset reads to test pipeline (to INTEGER)."
    echo "   -c --cores INTEGER"
    echo "          Number of cores to use (INTEGER)."
    echo "   -m --monitor"
    echo "          Monitor CPU and RAM."
    echo "   --shrink-fileshare"
    echo "          When finished, reduce quota of fileshare to be only 10G more than the total"
    echo "          space needed for the results. This option is not to be used when other tasks"
    echo "          are running on the same fileshare (e.g. when running different strategies"
    echo "          concomitantly."
    echo "   --input-samba-credentials FILE"
    echo "          File with the samba credentials to mount input fileshare (fastqs)."
    echo "   --output-samba-credentials FILE"
    echo "          File with the samba credentials to mount output fileshare (results)."
    echo "There should be files in the /etc/smbcredentials/ folder named <STORAGE_ACCT_FASTQ>.cred and <STORAGE_ACCT_RESULTS>.cred (the storage account can be the same for both input fastqs and results, then only one file is needed). This/these file/s contain the username=... and the password=... lines with the storage account name and the key."
    echo ""
    echo "For the results, a new fileshare will be created for the sample analysed, whose name will be a concatenation of the form <FILESHARE_RESULTS_PREFIX>-<SAMPLE_ID>."
    echo ""
    echo "EXAMPLE: $0 -s SL284405 -i forneurosysmedutvcompute/wgbsfqdata -o forneurosysmedutvcompute/prefix"
}

###### PARSE CMDLINE
psrecord="/home/gsnido.local/bin/psrecord"

sample_id=
storageAccountFastq=
shareNameFastq=
storageAccountResults=
shareNameResults=
task_id=
FORCE=0
SUBSET=0
SHRINK=0
MONITOR=0
cores=1

smbCredFastq=
smbCredResults=

while [ "$1" != "" ]; do
    case $1 in
        -s | --sample-id )               shift
                                         sample_id=$1
                                         ;;
        -t | --task-id )                 shift
                                         task_id=$1
                                         ;;
        -i | --input-storage-account )   shift
                                         storageAccountFastq=${1%/*}
                                         shareNameFastq=${1#*/}
                                         ;;
        -o | --output-storage-account )  shift
                                         storageAccountResults=${1%/*}
                                         shareNameResults=${1#*/}
                                         ;;
        --input-samba-credentials )      shift
                                         smbCredFastq=$1
                                         ;;
        --output-samba-credentials )     shift
                                         smbCredResults=$1
                                         ;;
        -f | --force )                   FORCE=1
                                         ;;
        -u | --subset )                  shift
                                         SUBSET=$1
                                         ;;
        -c | --cores )                   shift
                                         cores=$1
                                         ;;
        -m | --monitor )                 MONITOR=1
                                         ;;
        --shrink-fileshare )             SHRINK=1
                                         ;;
        -h | --help )                    usage
                                         exit
                                         ;;
        * )                              usage
                                         exit 1
    esac
    shift
done

if [[ "$sample_id" == "" ]]; then
    echo "[ERROR] -s option is mandatory."
    exit 2
fi
if [[ "$task_id" == "" ]]; then
    echo "[ERROR] -t option is mandatory."
    exit 2
fi
possible_tasks=("1_1" "1_2" "1_3" "1_4" "1_5" "2_1" "2_2" "2_3" "2_4" "3_1" "3_2" "3_3" "3_4" "3_5" "3_6")
if [[ ! " ${possible_tasks[@]} " =~ " ${task_id} " ]]; then
    echo "[ERROR] task id (-t option) can only be one of the following: ${possible_tasks[@]}"
    exit 2
fi

if [[ "$storageAccountFastq" == "" ]]; then
    echo "[ERROR] -i option is mandatory, e.g. forneurosysmedutvcompute/wgbsfqdata"
    exit 2
fi
if [[ "$shareNameFastq" == "" ]]; then
    echo "[ERROR] -i option is mandatory, e.g. forneurosysmedutvcompute/wgbsfqdata"
    exit 2
fi
if [[ "$storageAccountResults" == "" ]]; then
    echo "[ERROR] -o option is mandatory, e.g. forneurosysmedutvcompute/prefix"
    exit 2
fi
if [[ "$shareNameResults" == "" ]]; then
    echo "[ERROR] -o option is mandatory, e.g. forneurosysmedutvcompute/prefix"
    exit 2
fi

if [[ "$SUBSET" == "" ]]; then
    echo "[ERROR] -u option needs an integer, e.g. -u 10000"
    exit 2
fi
re='^[0-9]+$'
if ! [[ $SUBSET =~ $re ]]; then
       echo "[ERROR] -u option needs and integer, e.g. -u 100000"
       exit 2
fi

shareNameResults=$( echo ${shareNameResults}-${sample_id} | tr '[:upper:]' '[:lower:]' )

if [[ "$smbCredFastq" == "" ]]; then
    smbCredFastq=/etc/smbcredentials/${storageAccountFastq}.cred
    echo "[WARNING] Using default samba credentials file path: $smbCredFastq"
fi
if [[ "$smbCredResults" == "" ]]; then
    smbCredResults=/etc/smbcredentials/${storageAccountResults}.cred
    echo "[WARNING] Using default samba credentials file path: $smbCredResults"
fi

ref_dir="${SCRIPTPATH}/../ref"
b38_ref="${ref_dir}/GRCh38"
nuc_ref="${ref_dir}/GRCh38_no_mt"
mt_ref="${ref_dir}/chrM"

echo
echo "INPUT / VARIABLES"
echo "================="
echo "- sample_id: ${sample_id}"
echo "- storageAccountFastq: ${storageAccountFastq}"
echo "- shareNameFastq: ${shareNameFastq}"
echo "- storageAccountResults: ${storageAccountResults}"
echo "- shareNameResults: ${shareNameResults}"
echo "- smbCredFastq: ${smbCredFastq}"
echo "- smbCredResults: ${smbCredResults}"
echo "- FORCE: $FORCE"
echo "- SUBSET: $SUBSET"
echo "- SHRINK: ${SHRINK}"
echo


echo "ACTUALLY DOING STUFF"
echo "===================="
# Check credential files exist
echo ">> Getting keys (needs sudo)..."
if [[ ! -s $smbCredFastq ]]; then
    echo "   [ERROR] File \"$smbCredFastq\" does not exist"
    exit 2
fi
if [[ ! -s $smbCredResults ]]; then
    echo "   [ERROR] File \"$smbCredResults\" does not exist"
    exit 2
fi

storageAccountKeyFastq=$( sudo grep '^password=' $smbCredFastq | sed 's/^password=//' )
storageAccountKeyResults=$( sudo grep '^password=' $smbCredResults | sed 's/^password=//' )

if [[ $storageAccountKeyFastq == "" || $storageAccountKeyResults == "" ]]; then
    echo "   [ERROR] Samba credentials need to have \"username\" and \"passoword\" fields"
    exit 2
fi

echo "   [DONE]"


echo ">> Preparing input file share... "
# Check that fastq file share exists
exists=$( az storage share exists --account-name ${storageAccountFastq} --account-key ${storageAccountKeyFastq} --name ${shareNameFastq} --output tsv )
if [[ $exists != "True" ]]; then
    echo "   [ERROR] File share \"${shareNameFastq}\" in storage account \"${storageAccountFastq}\" does not exist"
    exit 2
fi

fastq_folder="/fileshares/${storageAccountFastq}/${shareNameFastq}"

sudo mkdir -p ${fastq_folder}

# Check if is mounted
isMounted=$( { grep -cs "${fastq_folder}" /proc/mounts || true; } )

if [[ $isMounted != 0 ]]; then
    echo "  * Already mounted"
else
    # Mount it
    echo "  * Try to mount"
    sudo mount -t cifs //${storageAccountFastq}.file.core.windows.net/${shareNameFastq} \
        ${fastq_folder} \
        -o vers=3.0,credentials=/etc/smbcredentials/${storageAccountFastq}.cred,dir_mode=0755,file_mode=0644,serverino,gid=1000,uid=1000,forceuid,forcegid
    if [[ $? != 0 ]]; then
        echo "   [ERROR] Could not mount file share for FASTQ files"
        exit 2
    fi
fi

echo "   [DONE]"

echo "  * Input FASTQ file share mounted in \"${fastq_folder}\""

echo ">> Preparing output file share... "
# Check that results fileshare does NOT exist or issue warning and wait
exists=$( az storage share exists --account-name ${storageAccountResults} --account-key ${storageAccountKeyResults} --name "${shareNameResults}" --output tsv )
if [[ $exists == "False" ]]; then
    az storage share create \
        --account-name ${storageAccountResults} \
        --account-key ${storageAccountKeyResults} \
        --name "${shareNameResults}" \
        --quota 9216 \
        --output none
else
    if [[ $FORCE == 0 ]]; then
        echo "   [ERROR] File share \"${shareNameResults}\" in \"${storageAccountResults}\""
        echo "           ALREADY EXISTS!"
        echo "           If you don't mind potentially overwriting, use -f option"
        exit 2
    else
        quota=$( az storage share show --account-name ${storageAccountResults} --account-key ${storageAccountKeyResults} --name "${shareNameResults}" --output json --query properties.quota )
        if [[ $quota < 9216 ]]; then
            echo "   [WARNING] File share \"${shareNameResults}\" in \"${storageAccountResults}\""
            echo "             has a quota smaller than 9216G, resizing to 9216..."
            az storage share update --account-name ${storageAccountResults} --account-key ${storageAccountKeyResults} --name "${shareNameResults}" --quota 9216
        fi
    fi
fi


results_folder="/fileshares/${storageAccountResults}/${shareNameResults}"


sudo mkdir -p ${results_folder}

# Check if is mounted
isMounted=$( { grep -cs "${results_folder}" /proc/mounts || true; } )

if [[ $isMounted != 0 ]]; then
    echo "  * Already mounted"
else
    echo "  * Trying to mount..."
    USR_ID=$( id -u )
    GRP_ID=$( id -g )
    sudo mount -t cifs //${storageAccountResults}.file.core.windows.net/${shareNameResults} \
        ${results_folder} \
        -o vers=3.0,credentials=/etc/smbcredentials/${storageAccountResults}.cred,dir_mode=0777,file_mode=0666,serverino,gid=${USR_ID},uid=${GRP_ID},forceuid,forcegid
    
    if [[ $? != 0 ]]; then
        echo "   [ERROR] Could not mount file share for RESULTS"
        exit 2
    else
        cd ${results_folder}
    fi
fi

echo "   [DONE]"

echo "   * Results file share mounted in \"${results_folder}\""


# Get fastq file names...
echo ">> Getting FASTQ filenames... "

all_files=($(find ${fastq_folder}/*${sample_id}*.fastq.gz))
num_files=${#all_files[@]}
if [[ $num_files -eq 0 ]]; then
    echo "   [ERROR] No fastq files retrieved"
    exit 3
fi

echo "   [DONE]"
echo "  * Number of FASTQ files found: ${num_files}"

declare -a R1
declare -a R2

# get R1 files and sort them
for file in "${all_files[@]}"; do
    IFS='.' read -r -a array <<< "${file}"
    if [[ ${array[1]} == "R1" ]]; then
        echo "    - R1: ${file}"
        R1+=(${file})
    else
        echo "    - R2: ${file}"
        R2+=(${file})
    fi
done

sample_id_long=$( basename ${R1[0]%.R1.fastq.gz} )

# Comma-separate input files for Bismark command
printf -v R1 '%s,' "${R1[@]}"
R1=$( echo "${R1%,}" )
printf -v R2 '%s,' "${R2[@]}"
R2=$( echo "${R2%,}" )



subset_opt=""
if [[ $SUBSET > 0 ]]; then
    subset_opt=" -u ${SUBSET}"
    echo "SUBSETTING TO ${SUBSET} READS ONLY!!!!"
    echo
fi

results_folder_s=${results_folder}/${sample_id_long}
mkdir -p ${results_folder_s}
echo "  * Final results folder: \"${results_folder_s}\""


# Create TMP folders
tmp_dir1=${results_folder_s}/tmp1
tmp_dir2=${results_folder_s}/tmp2
tmp_dir3=${results_folder_s}/tmp3
mkdir -p ${tmp_dir1}
mkdir -p ${tmp_dir2}
mkdir -p ${tmp_dir3}

# RUNNIN' THE TASK
tfile=""
if [[ $task_id == "1_1" ]]; then
    tfile=$(mktemp /tmp/cmd.XXXXXXXXX)
    echo -ne "${bismark} \\
              ${ref_dir}/GRCh38 \\
              -1 ${R1} -2 ${R2} \\
              --multicore ${cores} \\
              -o ${results_folder_s} --prefix b38 \\
              --path_to_bowtie ${bowtie_path} \\
              --samtools_path ${samtools_path} \\
              --temp_dir ${tmp_dir1}${subset_opt}\n" > ${tfile}
elif [[ $task_id == "1_2" ]]; then
    tfile=$(mktemp /tmp/cmd.XXXXXXXXX)
    bamfile_list=""
    IFS=',' read -r -a array <<< "${R1}"
    for element in "${array[@]}"; do
        bamfile=$( basename ${element} )
        bamfile="${results_folder_s}/b38.${bamfile%.fastq.gz}_bismark_bt2_pe.bam"
        bamfile_list="${bamfile_list} ${bamfile}"
    done
    echo -ne "${samtools} cat \\
              -o ${results_folder_s}/b38_${sample_id_long}.bam \\
              ${bamfile_list}\n" > ${tfile}
elif [[ $task_id == "1_3" ]]; then
    tfile=$(mktemp /tmp/cmd.XXXXXXXXX)
    if test -f "${results_folder_s}/b38_${sample_id_long}.deduplication_report.txt"; then
        echo "  * Report exists, deleting..."
        rm ${results_folder_s}/b38_${sample_id_long}.deduplication_report.txt
        echo "    Deletion error code: $?"
    fi
    echo -ne "${deduplicate_bismark} \\
              --paired \\
              --samtools_path ${samtools_path} \\
              --output_dir ${results_folder_s} \\
              ${results_folder_s}/b38_${sample_id_long}.bam\n
mv ${results_folder_s}/b38_${sample_id_long}.deduplicated.bam \\
              ${results_folder_s}/b38_${sample_id_long}.dedup.bam\n" > ${tfile}
elif [[ $task_id == "1_4" ]]; then
    tfile=$(mktemp /tmp/cmd.XXXXXXXXX)
    echo -ne "${bismark_meth_extract} -p \\
              --comprehensive \\
              --output ${results_folder_s} \\
              --parallel ${cores} \\
              --gzip \\
              --samtools_path ${samtools_path} \\
              ${results_folder_s}/b38_${sample_id_long}.dedup.bam\n" > ${tfile}
elif [[ $task_id == "1_5" ]]; then
    tfile=$(mktemp /tmp/cmd.XXXXXXXXX)
    echo -ne "zcat ${results_folder_s}/CpG_context_b38_${sample_id_long}.dedup.txt.gz | \\
              python3 ${ext_chr_srt} chrM | gzip -nc > \\
              ${results_folder_s}/CpG_context_chrM_${sample_id_long}.dedup.txt.gz\n
zcat ${results_folder_s}/CpG_context_b38_${sample_id_long}.dedup.txt.gz | \\
              python3 ${ext_chr_srt} chrL | gzip -nc > \\
              ${results_folder_s}/CpG_context_LAMBDA_${sample_id_long}.dedup.txt.gz\n
zcat ${results_folder_s}/CHH_context_b38_${sample_id_long}.dedup.txt.gz | \\
              python3 ${ext_chr_srt} chrM | gzip -nc > \\
              ${results_folder_s}/CHH_context_chrM_${sample_id_long}.dedup.txt.gz\n
zcat ${results_folder_s}/CHH_context_b38_${sample_id_long}.dedup.txt.gz | \\
              python3 ${ext_chr_srt} chrL | gzip -nc > \\
              ${results_folder_s}/CHH_context_LAMBDA_${sample_id_long}.dedup.txt.gz\n
zcat ${results_folder_s}/CHG_context_b38_${sample_id_long}.dedup.txt.gz | \\
              python3 ${ext_chr_srt} chrM | gzip -nc > \\
              ${results_folder_s}/CHG_context_chrM_${sample_id_long}.dedup.txt.gz\n
zcat ${results_folder_s}/CHG_context_b38_${sample_id_long}.dedup.txt.gz | \\
              python3 ${ext_chr_srt} chrL | gzip -nc > \\
              ${results_folder_s}/CHG_context_LAMBDA_${sample_id_long}.dedup.txt.gz\n
zcat ${results_folder_s}/C??_context_LAMBDA_${sample_id_long}.dedup.txt.gz | \\
              python3 ${meth_prop} > \\
              ${results_folder_s}/Lambda_meth_prop_${sample_id_long}.txt\n" > ${tfile}
elif [[ $task_id == "3_1" ]]; then
    tfile=$(mktemp /tmp/cmd.XXXXXXXXX)
    echo -ne "${bismark} \\
              ${ref_dir}/GRCh38_no_mt \\
              -1 ${R1} -2 ${R2} \\
              --multicore ${cores} \\
              --un --ambiguous \\
              -o ${results_folder_s} --prefix b38_noMT \\
              --path_to_bowtie ${bowtie_path} \\
              --samtools_path ${samtools_path} \\
              --temp_dir ${tmp_dir3}${subset_opt}\n" > ${tfile}
elif [[ $task_id == "3_2" ]]; then
    tfile=$(mktemp /tmp/cmd.XXXXXXXXX)
    unmapped_list_r1=""
    unmapped_list_r2=""
    IFS=',' read -r -a array <<< "${R1}"
    for element in "${array[@]}"; do
        uns=$( basename ${element} )
        uns="${results_folder_s}/b38_noMT.${uns}_unmapped_reads_1.fq.gz"
        unmapped_list_r1="${unmapped_list_r1} ${uns}"
    done
    IFS=',' read -r -a array <<< "${R2}"
    for element in "${array[@]}"; do
        uns=$( basename ${element} )
        uns="${results_folder_s}/b38_noMT.${uns}_unmapped_reads_2.fq.gz"
        unmapped_list_r2="${unmapped_list_r2} ${uns}"
    done
    
    echo -ne "cat ${unmapped_list_r1} > ${results_folder_s}/b38_noMT_${sample_id_long}_unmapped.R1.fastq.gz\n
rm ${unmapped_list_r1}\n
cat ${unmapped_list_r2} > ${results_folder_s}/b38_noMT_${sample_id_long}_unmapped.R2.fastq.gz\n
rm ${unmapped_list_r2}\n
rm ${results_folder_s}/b38_noMT.*_ambiguous_reads_?.fq.gz\n" > ${tfile}
elif [[ $task_id == "3_3" ]]; then
    tfile=$(mktemp /tmp/cmd.XXXXXXXXX)
    bamfile_list=""
    IFS=',' read -r -a array <<< "${R1}"
    for element in "${array[@]}"; do
        bamfile=$( basename ${element} )
        bamfile="${results_folder_s}/b38_noMT.${bamfile%.fastq.gz}_bismark_bt2_pe.bam"
        bamfile_list="${bamfile_list} ${bamfile}"
    done
    echo -ne "rm ${bamfile_list}\n" > ${tfile}
elif [[ $task_id == "3_4" ]]; then
    tfile=$(mktemp /tmp/cmd.XXXXXXXXX)
    echo -ne "${bismark} ${ref_dir}/chrM \\
              -1 ${results_folder_s}/b38_noMT_${sample_id_long}_unmapped.R1.fastq.gz \\
              -2 ${results_folder_s}/b38_noMT_${sample_id_long}_unmapped.R2.fastq.gz \\
              --multicore ${cores} \\
              -o ${results_folder_s} --prefix umap_to_MT \\
              --path_to_bowtie ${bowtie_path} \\
              --samtools_path ${samtools_path} \\
              --temp_dir ${tmp_dir3}\n
mv ${results_folder_s}/umap_to_MT.b38_noMT_${sample_id_long}_unmapped.R1_bismark_bt2_pe.bam ${results_folder_s}/umap_to_MT_${sample_id_long}.bam\n
mv ${results_folder_s}/umap_to_MT.b38_noMT_${sample_id_long}_unmapped.R1_bismark_bt2_PE_report.txt ${results_folder_s}/umap_to_MT_${sample_id_long}_report.txt\n
rm ${results_folder_s}/b38_noMT_${sample_id_long}_unmapped.R?.fastq.gz)\n" > ${tfile}
elif [[ $task_id == "3_5" ]]; then
    tfile=$(mktemp /tmp/cmd.XXXXXXXXX)
    echo -ne "${deduplicate_bismark} --paired \\
              --samtools_path ${samtools_path} \\
              --output_dir ${results_folder_s} \\
              ${results_folder_s}/umap_to_MT_${sample_id_long}.bam\n
mv ${results_folder_s}/umap_to_MT_${sample_id_long}.deduplicated.bam ${results_folder_s}/umap_to_MT_${sample_id_long}.dedup.bam\n" > ${tfile}
elif [[ $task_id == "3_6" ]]; then
    tfile=$(mktemp /tmp/cmd.XXXXXXXXX)
    echo -ne "${bismark_meth_extract} -p \\
              --comprehensive --output ${results_folder_s} \\
              --gzip --multicore ${cores} \\
              --samtools_path ${samtools_path} \\
              ${results_folder_s}/umap_to_MT_${sample_id_long}.dedup.bam\n" > ${tfile}
else
    echo "[ERROR] Task $task_id not yet implemented"
fi

echo ""
echo " %%%%%%%%%%%%%%%%%%%%%%%%% CMD %%%%%%%%%%%%%%%%%%%%%%%%"
echo /bin/bash ${tfile}
echo " %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
cat $tfile
echo " %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
echo  ""

if [[ $MONITOR == 1 ]]; then
    ${psrecord} "/bin/bash ${tfile}" --log ${log_dir}/${task_id}_psrecord.txt --interval 10 --include-children
else
    /bin/bash ${tfile}
fi

echo
echo "**************FINISHED**************"
echo


# Resize storage file share (CAREFUL IF OTHER TASKS NEED THE SPACE!)
if [[ $SHRINK > 0 ]]; then
    echo ">> Shrinking the fileshare..."
    # Get space used by files in share in GB
    sleep 30
    size1=$( az storage share stats --name ${shareNameResults} --account-name ${storageAccountResults} --account-key ${storageAccountKeyResults} )
    size2=$( du -sh -BG ${results_folder} | cut -f 1 | tr -d 'G' )
    size=$(( size1 > size2 ? size1 + 10 : size2 + 10))
    
    echo " Output files use around $(( size - 10 )) G"
    echo " Resizing fileshare quota to $size G..."
    
    az storage share update --name ${shareNameResults} --account-name ${storageAccountResults} --account-key ${storageAccountKeyResults} --quota ${size}
else
    echo "   * Fileshare in storage account left with original quota"
fi

echo 
cd ~
echo "   [DONE]"





